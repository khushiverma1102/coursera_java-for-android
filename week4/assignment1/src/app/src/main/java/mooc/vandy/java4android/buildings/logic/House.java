package mooc.vandy.java4android.buildings.logic;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

/**
 * This is the House class file that extends Building.
 */
public class House extends Building {

    private String  mOwner;
    private boolean mPool;

    public House(final int length, final int width, final int lotLength, final int lotWidth) {
        super(length, width, lotLength, lotWidth);
    }

    public House(final int length, final int width, final int lotLength, final int lotWidth, String owner) {
        super(length, width, lotLength, lotWidth);
        this.mOwner = owner;
    }

    public House(final int length, final int width, final int lotLength, final int lotWidth, String owner,
                 boolean pool) {
        super(length, width, lotLength, lotWidth);
        this.mOwner = owner;
        this.mPool = pool;
    }

    public String getOwner() {
        return mOwner;
    }

    public void setOwner(final String inMOwner) {
        this.mOwner = inMOwner;
    }

    public void setPool(final boolean inMPool) {
        this.mPool = inMPool;
    }

    public boolean hasPool() {
        return mPool;
    }

    @Override
    public boolean equals(final Object o) {
        House that = (House) o;
        return this.mPool == that.mPool && this.calcBuildingArea() == that.calcBuildingArea();
    }

    @Override
    public String toString() {
        ArrayList<String> result = new ArrayList<String>();

        if (mOwner.isEmpty()) {
            result.add("n/a");
        } else {
            result.add(String.format("Owner: %s", mOwner));
        }

        if (mPool) {
            result.add("has a pool");
        }

        if (calcBuildingArea() < calcLotArea()) {
            result.add("has a big open space");
        }
        return StringUtils.join(result, "; ");
    }
}

package mooc.vandy.java4android.calculator.logic;

/**
 * Perform the Multiply operation.
 */
public class Multiply implements MathOperation {

    /**
     * Multiply two numbers
     *
     * @param valueA
     * @param valueB
     * @return multiplication of two numbers.
     */
    public String calc(int valueA, int valueB) {
        return String.valueOf(valueA * valueB);
    }
}

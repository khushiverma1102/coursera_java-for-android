package mooc.vandy.java4android.calculator.logic;

/**
 * Perform the Divide operation.
 */
public class Divide implements MathOperation {

    /**
     * Divide two numbers
     *
     * @param valueA
     * @param valueB
     * @return division of two numbers or in case of @valueB == 0 than warning message.
     */
    public String calc(int valueA, int valueB) {
        if (valueB == 0) {
            return "Warning: division by 0 is not allowed.";
        }
        return String.valueOf(valueA / valueB) + " R: " + String.valueOf(valueA % valueB);
    }
}

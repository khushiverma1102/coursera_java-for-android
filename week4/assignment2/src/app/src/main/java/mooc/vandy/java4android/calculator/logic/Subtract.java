package mooc.vandy.java4android.calculator.logic;

/**
 * Perform the Subtract operation.
 */
public class Subtract implements MathOperation {

    /**
     * Subtract @valueB from @valueA
     *
     * @param valueA
     * @param valueB
     * @return subtraction of two numbers.
     */
    public String calc(int valueA, int valueB) {
        return String.valueOf(valueA - valueB);
    }
}
